﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CelularAutomata
{
    public class AutomataTexture : MonoBehaviour
    {
        MeshRenderer mr;
        Material mat;
        // m_Renderer.material.SetTexture("_MainTex", m_MainTexture);
        Color liveColor = new Color(1, 0, 0, 1);
        Color deadColor = new Color(0, 0, 0, 1);

        public int ecosystemSize = 32;
        public Texture2D map;

        public float iterationTime = 0.5f;
        public float iterationTimer;
        [Range(0, 100)]
        public int fillPercent = 50;
        public bool usePerlin = true;
        public float perlinScale = 0.5f;
        [Range(-1, 10)]
        public int iterationNumber = 5;
        public int seed = -1;

        CelularAutomata automata;

        [Header("Prefabs")]
        public GameObject floorPrefab;

        bool callDieFunctionDone = false;

        void Start()
        {
            automata = new CelularAutomata(2, ecosystemSize, CelularAutomata.CaveFillFunction, OnAutomataDie, seed, iterationNumber, fillPercent, usePerlin, perlinScale);

            iterationTimer = iterationTime;

            mr = GetComponent<MeshRenderer>();
            mat = mr.material;
            map = new Texture2D(ecosystemSize, ecosystemSize);
            map.name = "Automata Map";
            map.wrapMode = TextureWrapMode.Clamp;
            map.filterMode = FilterMode.Point;

            DrawAutomata();

            mat.SetTexture("_MainTex", map);
        }

        // Update is called once per frame
        void Update()
        {
            if (automata.ecosystemState == EcosystemState.Alive)
            {
                iterationTimer -= Time.deltaTime;

                if (iterationTimer <= 0)
                {
                    automata.AutomataUpdate(new List<System.Action<int, CellState, System.Action<CellState>>>() { CelularAutomata.Caves, CelularAutomata.CavesFillDeadPixels }, DrawAutomata);

                    iterationTimer = iterationTime;
                }
            }
            else
            {
                if(!callDieFunctionDone)
                {
                    OnAutomataDie(automata.ecosystem, automata.seed);
                    callDieFunctionDone = true;
                }
            }
        }

        void DrawAutomata()
        {
            for (int y = 0; y < ecosystemSize; y++)
            {
                for (int x = 0; x < ecosystemSize; x++)
                {
                    switch (automata.ecosystem[x, y])
                    {
                        case CellState.Alive:
                            map.SetPixel(x, y, liveColor);
                            break;
                        case CellState.Dead:
                            map.SetPixel(x, y, deadColor);
                            break;
                    }
                }
            }

            map.Apply();
        }

        public void OnAutomataDie(CellState[,] ecosystem, int seed)
        {
            //Build Scenario
            for (int y = 0; y < ecosystemSize; y++)
            {
                for (int x = 0; x < ecosystemSize; x++)
                {

                    if (ecosystem[x, y] == CellState.Alive)
                    {
                        GameObject.Instantiate(floorPrefab, new Vector3(x, 0, y), Quaternion.identity);
                    }

                    //int halfRange = 1;
                    //Neightbourings
                    /*
                    for (int offsetY = y - halfRange; offsetY <= y + halfRange; offsetY++)
                    {
                        for (int offsetX = x - halfRange; offsetX <= x + halfRange; offsetX++)
                        {
                            int localX = (offsetX + ecosystemSize) % ecosystemSize;
                            int localY = (offsetY + ecosystemSize) % ecosystemSize;

                            

                        }
                    }
                    */
                }
            }
        }
    }
}