﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UniPetri
{
    //This is the marker interface script for unity
    public class NodeInterface : MonoBehaviour
    {
        public Node node;

        bool isOnDrag = false;

        [Header("UI")]
        public Text nameText;
        public Text markerCountText;
        public Image nodeImage;

        public List<ConnectionInterface> connectionInterfaceList = new List<ConnectionInterface>();

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (node != null)
            {
                nameText.text = node.name;
                markerCountText.text = node.markerList.Count.ToString();
            }
        }

        public void Drag()
        {
            PetriEngine.instance.isDraging = true;
            GetComponent<RectTransform>().position = Input.mousePosition;
        }

        public void Drop()
        {
            PetriEngine.instance.isDraging = false;
        }

        public void Click(BaseEventData bed)
        {
            PointerEventData ped = (PointerEventData)bed;

            //Add Marker to a node
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (ped.button == PointerEventData.InputButton.Left) //Add Marker
                {
                    AddMarker();
                }

                if (ped.button == PointerEventData.InputButton.Right) //Subtract Marker
                {
                    SubtractMarker();
                }
            }
            else
            //Delete if over it and click dete
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                Delete();
            }
            else
            if (ped.button == PointerEventData.InputButton.Left)
            {
                if (PetriEngine.instance.isConnecting)
                {
                    if (!PetriEngine.instance.connectionDirection) //if the direction is false (Node <- Transition) then we can call an end connection, else we cant connect
                    {
                        //Connect
                        PetriEngine.instance.EndConnection(this);
                    }
                }
                else
                {
                    PetriEngine.instance.StartConnection(this);
                }
            }

            bed.Reset();
        }

        public void Enter()
        {
            PetriEngine.blockCreation = true;

            //Check if the connection is permited
            if (PetriEngine.instance.isConnecting)
            {
                if (PetriEngine.instance.connectionDirection)
                {
                    //Show bad connection feedback
                    nodeImage.color = Color.red;
                }
                else
                {
                    //Show good connection feedback
                    nodeImage.color = Color.green;
                }
            }
            else
            {
                nodeImage.color = Color.yellow;
            }

            //Delete if over it and click dete
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                nodeImage.color = Color.red;
            }

            //Add Marker to a node
            if(Input.GetKey(KeyCode.LeftShift))
            {
                nodeImage.color = Color.cyan;
            }
        }

        public void Exit()
        {
            PetriEngine.blockCreation = false;

            nodeImage.color = Color.white;
        }

        public void AddMarker()
        {
            this.node.AddMarker(1);
        }

        public void SubtractMarker()
        {
            this.node.RemoveMarker(1);
        }

        public void Delete()
        {
            //Delete all connection interfaces before deleting it self
            /*connectionInterfaceList.ForEach(delegate (ConnectionInterface connectionInterface)
            {
                connectionInterface.Delete();
            });*/

            for (int i = connectionInterfaceList.Count - 1; i >= 0; i--)
            {
                ConnectionInterface connectionInterfaceToDelete = connectionInterfaceList[i];
                connectionInterfaceList.Remove(connectionInterfaceToDelete);
                connectionInterfaceToDelete.Delete();
            }

            //Remove connections on the network
            node.RemoveConnections();
            PetriEngine.instance.currentNetwork.RemoveNode(this.node);

            Destroy(this.gameObject);
        }
    }
}