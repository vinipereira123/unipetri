﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UniPetri
{
    public class ConnectionInterface : MonoBehaviour
    {
        public Connection connection;
        public RectTransform rectTransform;
        public RectTransform arrowHead;
        public RectTransform circleHead;
        public List<Image> lineImageList = new List<Image>();
        public Text weightText;


        public NodeInterface nodeInterface;
        public TransitionInterface transitionInterface;

        public float lineWidth = 5;
        bool direction = true;

        private void Start()
        {
            rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, lineWidth);
            direction = PetriEngine.instance.connectionDirection ? true : false;
        }

        // Update is called once per frame
        void Update()
        {
            if (PetriEngine.instance.isConnecting)
            {
                foreach (Image line in lineImageList)
                {
                    if (line.raycastTarget)
                    {
                        line.raycastTarget = false;
                    }
                    else
                    {
                        break;
                    }
                };
            }
            else
            {
                foreach (Image line in lineImageList)
                {
                    if (!line.raycastTarget)
                    {
                        line.raycastTarget = true;
                    }
                    else
                    {
                        break;
                    }
                };
            }

            if (direction)
            {
                Vector3 to = Input.mousePosition;

                if (transitionInterface != null)
                {
                    to = Vector3.Lerp(nodeInterface.transform.position, transitionInterface.transform.position, 0.9f);
                }

                Vector3 differenceVector = to - nodeInterface.transform.position;

                float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;

                rectTransform.rotation = Quaternion.Euler(0, 0, angle);
                rectTransform.sizeDelta = new Vector2(differenceVector.magnitude, lineWidth);
                rectTransform.position = Vector3.Lerp(nodeInterface.transform.position, to, 0.5f);

                //Arrow Head
                arrowHead.position = Vector3.Lerp(nodeInterface.transform.position, ((transitionInterface != null)? transitionInterface.transform.position : Input.mousePosition), 0.91f);
                arrowHead.rotation = Quaternion.Euler(0, 0, angle + 90);

                //Circle Head
                circleHead.position = Vector3.Lerp(nodeInterface.transform.position, ((transitionInterface != null) ? transitionInterface.transform.position : Input.mousePosition), 0.5f);
            }
            else
            {
                Vector3 to = Input.mousePosition;

                if (nodeInterface != null)
                {
                    to = Vector3.Lerp(transitionInterface.transform.position, nodeInterface.transform.position, 0.9f);
                }

                Vector3 differenceVector = to - transitionInterface.transform.position;

                float angle = Mathf.Atan2(differenceVector.y, differenceVector.x) * Mathf.Rad2Deg;

                rectTransform.rotation = Quaternion.Euler(0, 0, angle);
                rectTransform.sizeDelta = new Vector2(differenceVector.magnitude, lineWidth);
                rectTransform.position = Vector3.Lerp(transitionInterface.transform.position, to, 0.5f);

                //Arrow Head
                arrowHead.position = Vector3.Lerp(transitionInterface.transform.position, ((nodeInterface != null) ? nodeInterface.transform.position : Input.mousePosition), 0.91f);
                arrowHead.rotation = Quaternion.Euler(0, 0, angle + 90);

                //Circle Head
                circleHead.position = Vector3.Lerp(transitionInterface.transform.position, ((nodeInterface != null) ? nodeInterface.transform.position : Input.mousePosition), 0.5f);

                //Weight Text
                weightText.transform.parent.position = Vector3.Lerp(transitionInterface.transform.position, ((nodeInterface != null) ? nodeInterface.transform.position : Input.mousePosition), 0.5f);
            }

            if (this.connection != null)
            {
                circleHead.gameObject.SetActive(this.connection.inhibitor);
                weightText.text = this.connection.weight.ToString();
            }

            //Update connection feedback
            //if (connection != null)
            //{
            //    arrowHead.gameObject.SetActive(!connection.inhibitor);
            //    circleHead.gameObject.SetActive(connection.inhibitor);
            //}
        }

        public void Click()
        {
            
            if (Input.GetKey(KeyCode.LeftControl))
            {
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    this.connection.SubtractWeight();
                }
                else
                {
                    this.connection.AddWeight();
                }
            }
            else
            {
                //Delete if over it and click dete
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    Delete();
                }

                //Toggle Inhibit
                if (Input.GetKey(KeyCode.LeftShift))
                {
                    ToggleInhibit();
                }
            }

            if (PetriEngine.instance.isConnecting)
            {
                PetriEngine.instance.CancelConnection();
            }
        }

        public void Enter()
        {
            PetriEngine.blockCreation = true;

            //Delete if over it and click dete
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                ChangeColor(Color.red);
            }
        }

        public void Delete()
        {
            //Visual Feedbacks
            nodeInterface.connectionInterfaceList.Remove(this);
            transitionInterface.connectionInterfaceList.Remove(this);

            connection.RemoveConnections();
            PetriEngine.instance.currentNetwork.RemoveConnection(this.connection);

            Destroy(this.gameObject);
        }

        public void ToggleInhibit()
        {
            this.connection.inhibitor = !this.connection.inhibitor;
        }

        public void Exit()
        {
            PetriEngine.blockCreation = false;

            ChangeColor(Color.white);
        }

        public void ChangeColor(Color color)
        {
            lineImageList.ForEach(delegate (Image line)
            {
                line.color = color;
            });
        }
    }
}
