﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace UniPetri
{
    public class TransitionInterface : MonoBehaviour
    {
        public Transition transition;

        public Image statusImage;
        public Text nameText;

        bool oldStatus = false;

        public List<ConnectionInterface> connectionInterfaceList = new List<ConnectionInterface>();

        // Update is called once per frame
        void Update()
        {
            if (transition != null)
            {
                if (PetriEngine.instance.isExecuteMode)
                {
                    //if (transition.status != oldStatus)
                    {
                        statusImage.color = (transition.status == true) ? Color.green : Color.red;
                        //oldStatus = transition.status;
                    }

                    transition.UpdateStatus();
                }

                nameText.text = transition.name;
            }
        }

        public bool CheckStatus()
        {
            if (transition != null)
            {
                return transition.UpdateStatus();
            }
            else
            {
                return false;
            }
        }

        public void Drag()
        {
            PetriEngine.instance.isDraging = true;
            GetComponent<RectTransform>().position = Input.mousePosition;
        }

        public void Drop()
        {
            PetriEngine.instance.isDraging = false;
        }

        public void Click(BaseEventData bed)
        {
            PointerEventData ped = (PointerEventData)bed;

            //if (Input.GetKey(KeyCode.LeftControl))
            if (ped.button == PointerEventData.InputButton.Left)
            {
                if (PetriEngine.instance.isConnecting)
                {
                    if (PetriEngine.instance.connectionDirection) //if the direction is true (Node -> Transition) then we can call an end connection, else we cant connect
                    {
                        //Connect
                        PetriEngine.instance.EndConnection(this);
                    }
                }
                else
                {
                    PetriEngine.instance.StartConnection(this);
                }
            }

            if (ped.button == PointerEventData.InputButton.Right)
            {
                //Delete if over it and click dete
                if (Input.GetKey(KeyCode.LeftAlt))
                {
                    Delete();
                }
            }

            bed.Reset();
        }

        public void Enter()
        {
            PetriEngine.blockCreation = true;

            //Check if the connection is permited
            if (PetriEngine.instance.isConnecting)
            {
                if (!PetriEngine.instance.connectionDirection)
                {
                    //Show bad connection feedback
                    statusImage.color = Color.red;
                }
                else
                {
                    //Show good connection feedback
                    statusImage.color = Color.green;
                }
            }
            else
            {
                if (!PetriEngine.instance.isExecuteMode)
                {
                    statusImage.color = Color.yellow;
                }
            }

            //Delete if over it and click dete
            if (Input.GetKey(KeyCode.LeftAlt))
            {
                statusImage.color = Color.red;
            }
        }

        public void Exit()
        {
            PetriEngine.blockCreation = false;

            if (!PetriEngine.instance.isExecuteMode)
            {
                statusImage.color = Color.white;
            }
        }

        public void Delete()
        {
            //Delete all connection interfaces before deleting it self
            /*connectionInterfaceList.ForEach(delegate (ConnectionInterface connectionInterface)
            {
                connectionInterface.Delete();
            });*/

            for (int i = connectionInterfaceList.Count - 1; i >= 0; i--)
            {
                ConnectionInterface connectionInterfaceToDelete = connectionInterfaceList[i];
                connectionInterfaceList.Remove(connectionInterfaceToDelete);
                connectionInterfaceToDelete.Delete();
            }

            //Remove connections on the network
            transition.RemoveConnections();
            PetriEngine.instance.currentNetwork.RemoveTransition(this.transition);

            Destroy(this.gameObject);
        }
    }
}
