﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UniPetri;
using UnityEngine.EventSystems;

public class PetriEngine : MonoBehaviour
{
    public static PetriEngine instance;

    public static bool blockCreation = false;

    [Header("UI")]
    public CanvasScaler canvasScaler;
    public Transform BG;
    public GameObject executeModeUI;
    public Text modeTitle;
    public GameObject UI;

    [Header("Prefabs")]
    public GameObject nodeInterfacePrefab;
    public GameObject transitionInterfacePrefab;
    public GameObject connectionInterfacePrefab;

    public PetriNetwork currentNetwork;

    float doubleClickTimer = 0;
    float doubleClickCooldown = 0.3f;

    public NodeInterface nodeInterfaceToConnect;
    public TransitionInterface transitionInterfaceToConnect;
    public GameObject currentConnection;

    public bool isDraging = false;
    public bool isConnecting = false;
    public bool connectionDirection = true;
    public bool isExecuteMode = false;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(instance.gameObject);
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        NewNetwork();
    }

    private void Update()
    {
        if(isExecuteMode)
        {
            if(!executeModeUI.activeSelf)
            {
                executeModeUI.SetActive(true);
            }
        }
        else
        {
            if (executeModeUI.activeSelf)
            {
                executeModeUI.SetActive(false);
            }
        }

        if(isConnecting)
        {
            blockCreation = true;

            if(Input.GetKey(KeyCode.Delete))
            {
                CancelConnection();
            }
        }
        else
        {
            //We check if the connection exists, and of it is connected or not, if not we delete it
            if(currentConnection != null)
            {
                ConnectionInterface connectionInterface = currentConnection.GetComponent<ConnectionInterface>();
                if(connectionInterface.nodeInterface == null || connectionInterface.transitionInterface == null)
                {
                    connectionInterface.Delete();
                }
                else
                {
                    currentConnection = null;
                }
            }

            blockCreation = false;
        }


        //Keep the BG as first sibling
        if(BG.transform.GetSiblingIndex() != 0)
        {
            BG.transform.SetAsFirstSibling();
        }

        //Keep the UI as last sibling
        if (UI.transform.GetSiblingIndex() != transform.childCount - 1)
        {
            UI.transform.SetAsLastSibling();
        }
    }

    #region Functions
    public void LeftDoubleClick()
    {
        if (!blockCreation)
        {
            CreateNode(Input.mousePosition);
        }
    }

    public void RightDoubleClick()
    {
        if (!blockCreation)
        {
            CreateTransition(Input.mousePosition);
        }
    }

    public void Click(BaseEventData bed)
    {
        if (!isExecuteMode)
        {
            /*if (doubleClickTimer > 0)
            {
                doubleClickTimer -= Time.deltaTime;
            }*/

            PointerEventData ped = (PointerEventData)bed;

            if (ped.button == PointerEventData.InputButton.Left)
            {
                /*if (doubleClickTimer <= 0)
                {
                    doubleClickTimer = doubleClickCooldown;
                }
                else*/
                {
                    LeftDoubleClick();
                }
            }

            if (ped.button == PointerEventData.InputButton.Right)
            {
                /*if (doubleClickTimer <= 0)
                {
                    doubleClickTimer = doubleClickCooldown;
                }
                else*/
                {
                    RightDoubleClick();
                }
            }

            if (isConnecting)
            {
                CancelConnection();
            }

            bed.Reset();
        }
    }

    public void ExecuteMode()
    {
        isExecuteMode = true;
        modeTitle.text = "Execute Mode";
    }

    public void EditMode()
    {
        isExecuteMode = false;
        modeTitle.text = "Edit Mode";
    }

    #endregion

    #region Network
    public void NewNetwork()
    {
        currentNetwork = new PetriNetwork();
    }

    public void SaveNetwork()
    {

    }

    public void LoadNetwork()
    {

    }
    #endregion

    #region Menu
    public void CreateNode(Vector3 position)
    {
        GameObject newNode = Instantiate(nodeInterfacePrefab, position, Quaternion.identity, transform);
        NodeInterface nodeInterface = newNode.GetComponent<NodeInterface>();

        nodeInterface.node = currentNetwork.CreateNode();
    }

    public void CreateTransition(Vector3 position)
    {
        Vector3 newPos = new Vector3(position.x, position.y, 50);
        GameObject newTransition = Instantiate(transitionInterfacePrefab, position, Quaternion.identity, transform);
        TransitionInterface transitionInterface = newTransition.GetComponent<TransitionInterface>();

        transitionInterface.transition = currentNetwork.CreateTransition();
    }

    public void StartConnection(NodeInterface nodeInterface)
    {
        if (!isDraging && !isExecuteMode)
        {
            isConnecting = true;
            connectionDirection = true;
            nodeInterfaceToConnect = nodeInterface;

            currentConnection = Instantiate(connectionInterfacePrefab, transform);

            ConnectionInterface connectionInterface = currentConnection.GetComponent<ConnectionInterface>();
            connectionInterface.transform.SetAsFirstSibling();
            connectionInterface.nodeInterface = nodeInterface;
        }
    }

    public void StartConnection(TransitionInterface transitionInterface)
    {
        if (!isDraging)
        {
            isConnecting = true;
            connectionDirection = false;
            transitionInterfaceToConnect = transitionInterface;

            currentConnection = Instantiate(connectionInterfacePrefab, transform);
            currentConnection.transform.SetAsFirstSibling();

            ConnectionInterface connectionInterface = currentConnection.GetComponent<ConnectionInterface>();
            connectionInterface.transitionInterface = transitionInterface;
        }
    }

    public void EndConnection(NodeInterface nodeInterface)
    {
        isConnecting = false;

        ConnectionInterface connectionInterface = currentConnection.GetComponent<ConnectionInterface>();

        connectionInterface.connection = nodeInterface.node.ConnectTo(connectionInterface.transitionInterface.transition);
        connectionInterface.nodeInterface = nodeInterface;

        nodeInterface.connectionInterfaceList.Add(connectionInterface);

        ClearConnection();
    }

    public void EndConnection(TransitionInterface transitionInterface)
    {
        ConnectionInterface connectionInterface = currentConnection.GetComponent<ConnectionInterface>();

        connectionInterface.connection = transitionInterface.transition.ConnectTo(connectionInterface.nodeInterface.node);
        connectionInterface.transitionInterface = transitionInterface;

        transitionInterface.connectionInterfaceList.Add(connectionInterface);

        ClearConnection();
    }

    public void CancelConnection()
    {
        isConnecting = false;
        nodeInterfaceToConnect = null;
        transitionInterfaceToConnect = null;
        Destroy(currentConnection.gameObject);
        currentConnection = null;
    }

    public void ClearConnection()
    {
        isConnecting = false;
        nodeInterfaceToConnect = null;
        currentConnection = null;
        transitionInterfaceToConnect = null;
    }

    public void Execute()
    {
        currentNetwork.Execute();
    }

    public void CheckTransitions()
    {
        currentNetwork.CheckTransitions();
    }

    public void CheckNetwork()
    {
        currentNetwork.CheckNetwork();
    }

    #endregion
}
