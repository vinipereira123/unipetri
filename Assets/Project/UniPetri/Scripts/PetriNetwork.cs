﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace UniPetri
{
    #region Utility
    public static class Utility
    {
        static public string GetRandomId(int size)
        {
            System.Random rnd = new System.Random(System.DateTime.Now.Millisecond);
            string chars = "abcdefghijklmnopqrstuvxywzABCDEFGHIJKLMNOPQRSTUVXYWZ0123456789";

            string newId = "";
            for (int i = 0; i < size; i++)
            {
                newId += chars[rnd.Next(0, (chars.Length - 1))];
            }

            return newId;
        }
    }
    #endregion

    #region Marker
    public class Marker
    {
        public Node parentNode;
        public GameObject objectReference;
        public Action<List<Connection>> concurrecyHandlerFunction;
        public Transition chosenTransition;
        public Action changeParentNodeFunction;

        public Marker(Node pNode)
        {
            //Empty marker just for generic uses of the network
            this.concurrecyHandlerFunction = DefaultRandomConcurrencyHandler;
        }

        public Marker(Node pNode, GameObject obj, Action<List<Connection>> cFunction, Action cParentNodeFunction)
        {
            this.objectReference = obj;
            this.concurrecyHandlerFunction = cFunction;
            this.changeParentNodeFunction = cParentNodeFunction;
            this.parentNode = pNode;
        }

        public void SetParentNode(Node pNode)
        {
            if(this.changeParentNodeFunction != null)
            {
                this.changeParentNodeFunction.Invoke();
            }

            this.parentNode = pNode;
        }

        public void DefaultNonConcurrencyHandler(Connection connection)
        {
            this.chosenTransition = null;
            this.chosenTransition = connection.transition;
            this.chosenTransition.markerToMove.Add(this);
        }

        public void DefaultConcurrencyHandler(List<Connection> connectionList)
        {
            //Return that the first connection is the one to be chosen
            this.chosenTransition = null;
            this.chosenTransition = connectionList[0].transition;
            this.chosenTransition.markerToMove.Add(this);
        }

        public void DefaultRandomConcurrencyHandler(List<Connection> connectionList)
        {
            this.chosenTransition = null;
            this.chosenTransition = connectionList[UnityEngine.Random.Range(0, connectionList.Count - 1)].transition;
            this.chosenTransition.markerToMove.Add(this);
        }

        public void SelfRemove()
        {
            parentNode.RemoveMarker(this);
        }
    }
    #endregion

    #region Connection
    public class Connection
    {
        public bool inhibitor = false;
        public bool direction; //true = Node -> Transition | false = Node <- Transition
        public int weight = 1;
        public Node node;
        public Transition transition;

        public Connection(Node _node, Transition _transition, int _weight = 1, bool isInhibitor = false)
        {
            this.node = _node;
            this.transition = _transition;
            this.direction = true;
            this.inhibitor = isInhibitor;
            this.weight = _weight;
        }

        public Connection(Transition _transition, Node _node, int _weight = 1, bool isInhibitor = false)
        {
            this.node = _node;
            this.transition = _transition;
            this.direction = false;
            this.inhibitor = isInhibitor;
            this.weight = _weight;
        }

        public void RemoveConnections()
        {
            if (this.direction)//Node -> Transition
            {
                this.node.outputConnectionList.Remove(this);
                this.transition.inputConnectionList.Remove(this);
            }
            else //Node <- Transition
            {
                this.transition.outputConnectionList.Remove(this);
                this.node.inputConnectionList.Remove(this);
            }
        }

        public void Inhibit()
        {
            this.inhibitor = !this.inhibitor;
        }

        public void SetWeight(int _weight)
        {
            this.weight = _weight;
        }

        public void AddWeight()
        {
            this.weight++;
        }

        public void SubtractWeight()
        {
            if (this.weight - 1 > 0)
            {
                this.weight--;
            }
        }
    }
    #endregion

    #region ConcurrencyDecision
    /*public class ConcurrencyDecision
    {
        public Marker markerToMove;
        public Transition chosenTransition;
    }*/
    #endregion

    #region Node
    public class Node
    {
        public PetriNetwork parentNetwork;
        public string uniqueId;
        public string name;
        //public int markerCount;
        public List<Marker> markerList = new List<Marker>();
        public List<Transition> concurrencyTransitionUniqueIdList = new List<Transition>();
        //public List<ConcurrencyDecision> concurrencyDecisionList = new List<ConcurrencyDecision>();
        public bool hasConcurrency = false;
        public List<Connection> inputConnectionList = new List<Connection>();
        public List<Connection> outputConnectionList = new List<Connection>();
        public List<Action> onAddCallbackList = new List<Action>();
        public List<Action> onExecuteCallbackList = new List<Action>();
        public List<Action> onSubtractCallbackList = new List<Action>();
        public Action<List<Transition>, Transition> onConcurrency;
        public List<string> concurrentTransitionUniqueIdList = new List<string>();
        public MapTile mapTileReference;

        //Node handling
        public Node(int _id, string _name, PetriNetwork parent)
        {
            this.parentNetwork = parent;
            this.name = _name;
        }

        public Connection ConnectTo(Transition transition)
        {
            //New Connection
            Connection newConnection = new Connection(this, transition);
            //Add conection as output
            this.inputConnectionList.Add(newConnection);
            //Add connection as input in the transition
            transition.outputConnectionList.Add(newConnection);

            return newConnection;
        }

        public void CheckConcurrency()
        {
            concurrencyTransitionUniqueIdList.Clear();
            //concurrencyDecisionList.Clear();

            if (outputConnectionList.Count > 1)
            {
                hasConcurrency = true;

                //Run concurrency function for each marker
                foreach (Marker marker in markerList)
                {
                    marker.concurrecyHandlerFunction.Invoke(outputConnectionList);
                    concurrencyTransitionUniqueIdList.Add(marker.chosenTransition);
                }


                //Run the concurrency function and gets the prefered transition list
                /*parentNetwork.concurrecyHandlerFunction.Invoke(outputConnectionList, delegate (Transition transition)
                {
                    concurrencyTransitionUniqueIdList.Add(transition);

                });*/

                //Active concurrency override
                List<Connection> outputNotInhibitedConnectionList = outputConnectionList.FindAll(x => x.inhibitor == false);
                outputNotInhibitedConnectionList.ForEach(delegate (Connection connection)
                {
                    if (concurrencyTransitionUniqueIdList.Contains(connection.transition))
                    {
                        connection.transition.hasConcurrencyOverride = true;
                        connection.transition.status = true;
                    }
                    else
                    {
                        connection.transition.hasConcurrencyOverride = false;
                        connection.transition.status = false;
                    }
                });
            }
            else
            {
                hasConcurrency = false;

                if (outputConnectionList.Count > 0)
                {
                    foreach (Marker marker in markerList)
                    {
                        marker.DefaultNonConcurrencyHandler(outputConnectionList[0]);
                    }
                }
            }
        }

        public void RemoveConnections()
        {
            //Remove Inputs
            for (int i = 0; i < this.inputConnectionList.Count; i++)
            {
                this.inputConnectionList[i].RemoveConnections();
            }

            //Remove Outputs
            for (int i = 0; i < this.outputConnectionList.Count; i++)
            {
                this.outputConnectionList[i].RemoveConnections();
            }
        }

        //Marker handling
        public int SetMarkers(List<Marker> newMarkerList) //Overwrites the existing markers list
        {
            this.markerList.Clear();
            this.markerList = new List<Marker>(newMarkerList);

            return this.markerList.Count;
        }

        public int SetMarker(Marker newMarker)
        {
            this.markerList.Clear();

            this.markerList.Add(newMarker);

            return this.markerList.Count;
        }

        public int SetMarker(int quantity)
        {
            this.markerList.Clear();

            for (int i = 0; i < quantity; i++)
            {
                this.markerList.Add(new Marker(this));
            }

            return this.markerList.Count;
        }

        public int AddMarker(Marker newMarker)
        {
            this.markerList.Add(newMarker);

            foreach (Action callback in onAddCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public int AddMarkers(List<Marker> newMarkers)
        {
            for (int i = 0; i < newMarkers.Count; i++)
            {
                newMarkers[i].SetParentNode(this);
                this.markerList.Add(newMarkers[i]);
            }

            foreach (Action callback in onAddCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public int AddMarker(int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                this.markerList.Add(new Marker(this));
            }

            foreach (Action callback in onAddCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public int RemoveMarker(Marker marker)
        {
            this.markerList.Remove(marker);

            foreach (Action callback in onSubtractCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public int RemoveMarkers(List<Marker> markerList)
        {
            for (int i = 0; i < markerList.Count; i++)
            {
                markerList[i].SetParentNode(null);
                this.markerList.Remove(markerList[i]);
            }

            foreach (Action callback in onSubtractCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public void ClearMarker()
        {
            this.markerList.Clear();
        }

        public int RemoveMarker(int quantity)
        {
            this.markerList.RemoveRange(0, quantity);

            foreach (Action callback in onSubtractCallbackList)
            {
                callback.Invoke();
            }

            return this.markerList.Count;
        }

        public void Execute()
        {
            if (this.markerList.Count > 0)
            {
                foreach (Action callback in onExecuteCallbackList)
                {
                    callback.Invoke();
                }
            }
        }

    }
    #endregion

    #region Transition
    public class Transition
    {
        public string uniqueId;
        public string name;
        public bool status;
        public bool hasConcurrencyOverride = false;
        public List<Connection> inputConnectionList = new List<Connection>();
        public List<Connection> outputConnectionList = new List<Connection>();
        public List<Action> callbackList = new List<Action>();

        public List<Marker> markerToMove = new List<Marker>();

        public Transition(string _name = "")
        {
            this.name = _name;
        }

        public Connection ConnectTo(Node node)
        {
            //New Connection
            Connection newConnection = new Connection(this, node);
            //Add conection as output
            this.inputConnectionList.Add(newConnection);
            //Add connection as input in the node
            node.outputConnectionList.Add(newConnection);

            return newConnection;
        }

        public void RemoveConnections()
        {
            //Remove Inputs
            for (int i = 0; i < this.inputConnectionList.Count; i++)
            {
                this.inputConnectionList[i].RemoveConnections();
            }

            //Remove Outputs
            for (int i = 0; i < this.outputConnectionList.Count; i++)
            {
                this.outputConnectionList[i].RemoveConnections();
            }
        }

        public bool UpdateStatus()
        {
            this.status = true;

            //Check if all connections are satisfied
            List<Connection> notInhibitedConnectionList = inputConnectionList.FindAll(x => x.inhibitor == false);
            for (int i = 0; i < notInhibitedConnectionList.Count; i++)
            {
                if (notInhibitedConnectionList[i].weight > notInhibitedConnectionList[i].node.markerList.Count) //If any of the connections are not satisfied we cant proceed
                {
                    this.status = false;
                }
            }

            if (hasConcurrencyOverride)
            {
                List<Connection> hasUniqueIdInputList = notInhibitedConnectionList.FindAll(x => x.node.concurrencyTransitionUniqueIdList.Contains(this));
                List<Connection> hasMarkerInputList = hasUniqueIdInputList.FindAll(x => x.node.markerList.Count > 0);

                if (hasUniqueIdInputList.Count > 0 && hasMarkerInputList.Count > 0)
                {
                    if (hasUniqueIdInputList.Count - hasMarkerInputList.Count == 0) //If == 0 the lists have the same count, therefore they are all with markes and the transition can proceed
                    {
                        this.status = true;
                    }
                    else
                    {
                        this.status = false;
                    }
                }
                else
                {
                    this.status = false;
                }
            }

            notInhibitedConnectionList.Clear();

            //Check inhibitors connections
            List<Connection> inhibitorConnectionList = inputConnectionList.FindAll(x => x.inhibitor == true);

            bool originalValue = this.status;
            if (inhibitorConnectionList.Count > 0)
            {
                List<Connection> activeInhibitorConnectionList = inhibitorConnectionList.FindAll(x => x.node.markerList.Count > 0);

                if (activeInhibitorConnectionList.Count > 0)
                {
                    if (this.status)
                    {
                        this.status = false;
                    }
                    else
                    {
                        this.status = true;
                    }
                }
                else
                {
                    this.status = originalValue;
                }

                activeInhibitorConnectionList.Clear();
            }
            else
            {
                this.status = originalValue;
            }

            inhibitorConnectionList.Clear();

            return status;
        }

        public void Execute(bool transferMarker = false)
        {
            //If the status is true we can execute the transition
            if (status)
            {
                if (transferMarker)
                {
                    //Execute callback functions
                    foreach (Action callback in callbackList)
                    {
                        callback.Invoke();
                    }

                    //Execute marker extraction and contraction
                    List<Connection> nonInhibitedCOnnectionList = inputConnectionList.FindAll(x => x.inhibitor == false);
                    //Remove markers from input nodes
                    foreach (Connection connection in nonInhibitedCOnnectionList)
                    {
                        connection.node.RemoveMarker(connection.weight);
                    }

                    //Add one marker on all output nodes
                    foreach (Connection connection in outputConnectionList)
                    {
                        connection.node.AddMarker(connection.weight);
                    }

                    //Remove reference of the nodes in the transition
                    foreach (Connection connection in nonInhibitedCOnnectionList)
                    {
                        connection.transition.markerToMove.Clear();
                        //connection.transition.markerToMove.Clear();
                    }
                }
                else
                {
                    //Execute callback functions
                    foreach (Action callback in callbackList)
                    {
                        callback.Invoke();
                    }

                    //Execute marker migration
                    for (int j = 0; j < this.inputConnectionList.Count; j++)
                    {
                        inputConnectionList[j].node.RemoveMarkers(this.markerToMove);
                    }

                    for (int j = 0; j < this.outputConnectionList.Count; j++)
                    {
                        outputConnectionList[j].node.AddMarkers(this.markerToMove);
                    }

                    this.markerToMove.Clear();

                    /*List<Connection> nonInhibitedCOnnectionList = inputConnectionList.FindAll(x => x.inhibitor == false);
                    List<Marker> markerToTransferList = new List<Marker>();
                    //Remove markers from input nodes
                    foreach (Connection connection in nonInhibitedCOnnectionList)
                    {
                        markerToTransferList = new List<Marker>(connection.transition.markerToMove);

                        connection.node.RemoveMarkers(connection.transition.markerToMove);
                        //connection.transition.markerToMove.Clear();
                    }

                    //Add one marker on all output nodes
                    foreach (Connection connection in outputConnectionList)
                    {
                        connection.node.AddMarkers(markerToTransferList);
                    }

                    //Remove reference of the nodes in the transition
                    foreach (Connection connection in nonInhibitedCOnnectionList)
                    {
                        connection.transition.markerToMove.Clear();
                        //connection.transition.markerToMove.Clear();
                    }*/
                }

                //Reset concurrency status as we just solved it
                if (this.hasConcurrencyOverride)
                {
                    this.hasConcurrencyOverride = false;
                }
            }
        }
    }
    #endregion

    #region Network
    public class PetriNetwork
    {
        public List<Connection> connectionList = new List<Connection>();
        public List<Node> nodeList = new List<Node>();
        public List<Transition> transitionList = new List<Transition>();
        public Action<List<Connection>, Action<Transition>> concurrecyHandlerFunction;
        public List<Marker> markerList = new List<Marker>();

        public bool stopExecution = false;

        public PetriNetwork()
        {
            //this.concurrecyHandlerFunction = concurrecyFunction;
        }

        //Node Handling
        public Node CreateNode(MapTile mapTile, string name = "")
        {
            //Name
            if (name == "")
            {
                name = "Node_" + this.nodeList.Count.ToString();
            }

            //Unique Id
            string uniqueId = Utility.GetRandomId(4);
            while (nodeList.Find(x => x.uniqueId == uniqueId) != null)
            {
                uniqueId = Utility.GetRandomId(4);
            }

            //Data
            Node newNode = new Node(nodeList.Count, name, this);
            newNode.uniqueId = uniqueId;
            this.nodeList.Add(newNode);
            newNode.mapTileReference = mapTile;
            return newNode;
        }

        public Node CreateNode(string name = "")
        {
            //Name
            if (name == "")
            {
                name = "Node_" + this.nodeList.Count.ToString();
            }

            //Unique Id
            string uniqueId = Utility.GetRandomId(4);
            while (nodeList.Find(x => x.uniqueId == uniqueId) != null)
            {
                uniqueId = Utility.GetRandomId(4);
            }

            //Data
            Node newNode = new Node(nodeList.Count, name, this);
            newNode.uniqueId = uniqueId;
            this.nodeList.Add(newNode);
            return newNode;
        }

        public void RemoveNode(Node nodeToRemove)
        {
            if (nodeToRemove != null)
            {
                nodeToRemove.RemoveConnections();
                nodeList.Remove(nodeToRemove);
            }
        }

        public void RemoveNode(string name)
        {
            Node nodeToRemove = nodeList.Find(x => x.name == name);
            if (nodeToRemove != null)
            {
                nodeToRemove.RemoveConnections();
                nodeList.Remove(nodeToRemove);
            }
        }

        public Node GetRandomNode()
        {
            Node node = nodeList[UnityEngine.Random.Range(0, nodeList.Count - 1)];

            while (!node.mapTileReference.active)
            {
                node = nodeList[UnityEngine.Random.Range(0, nodeList.Count - 1)];
            }

            return node;
        }

        //Transition Handling
        #region TransitionHandling
        public Transition CreateTransition(string name = "")
        {
            if (name == "")
            {
                name = "Transition_" + transitionList.Count;
            }

            //Unique Id
            string uniqueId = Utility.GetRandomId(4);
            while (nodeList.Find(x => x.uniqueId == uniqueId) != null)
            {
                uniqueId = Utility.GetRandomId(4);
            }

            Transition newTransition = new Transition(name);
            newTransition.uniqueId = uniqueId;
            this.transitionList.Add(newTransition);
            return newTransition;
        }

        public void RemoveTransition(Transition transitionToRemove)
        {
            if (transitionToRemove != null)
            {
                transitionToRemove.RemoveConnections();
                this.transitionList.Remove(transitionToRemove);
            }
        }

        public void RemoveTransition(string name)
        {
            Transition transitionToRemove = this.transitionList.Find(x => x.name == name);
            if (transitionToRemove != null)
            {
                transitionToRemove.RemoveConnections();
                this.transitionList.Remove(transitionToRemove);
            }
        }
        #endregion

        //Connection Handling
        #region ConnectionHandling
        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="transition"></param>
        /// <param name="direction">true = Node -> Transition | false = Transition -> Node</param>
        /// <param name="weight"></param>
        /// <param name="isInhibitor"></param>
        public void CreateConnection(Node node, Transition transition, bool direction = true, int weight = 1, bool isInhibitor = false)
        {
            if (direction) //Node -> Transition
            {
                Connection newConnection = node.ConnectTo(transition);
                newConnection.weight = weight;
                newConnection.inhibitor = isInhibitor;
                this.connectionList.Add(newConnection);
            }
            else //Node <- Transition
            {
                Connection newConnection = transition.ConnectTo(node);
                newConnection.weight = weight;
                newConnection.inhibitor = isInhibitor;
                this.connectionList.Add(newConnection);
            }
        }

        //Node 2 Node
        public void CreateConnection(Node nodeFrom, Node nodeTo)
        {
            Transition newTransition = CreateTransition();

            CreateConnection(nodeFrom, newTransition, true);
            CreateConnection(nodeTo, newTransition, false);
        }

        public void CreateConnection(string nodeName, string transitionName, bool direction = true, int weight = 1, bool isInhibitor = false)
        {
            if (direction) //Node -> Transition
            {
                Connection newConnection = nodeList.Find(x => x.name == nodeName).ConnectTo(transitionList.Find(x => x.name == transitionName));
                newConnection.weight = weight;
                newConnection.inhibitor = isInhibitor;
                this.connectionList.Add(newConnection);
            }
            else //Node <- Transition
            {
                Connection newConnection = transitionList.Find(x => x.name == transitionName).ConnectTo(nodeList.Find(x => x.name == nodeName));
                newConnection.weight = weight;
                newConnection.inhibitor = isInhibitor;
                this.connectionList.Add(newConnection);
            }
        }


        public void RemoveConnection(Connection connectionToRemove)
        {
            connectionToRemove.RemoveConnections();
            this.connectionList.Remove(connectionToRemove);
        }

        public void RemoveConnection(string nodeName, string transitionName, bool direction = true)
        {
            Node node = nodeList.Find(x => x.name == nodeName);
            Transition transition = transitionList.Find(x => x.name == transitionName);

            if (direction) //Node -> Transition
            {
                Connection connectionToRemove = node.outputConnectionList.Find(x => x.transition == transition);
                if (connectionToRemove != null)
                {
                    connectionToRemove.RemoveConnections();
                    this.connectionList.Remove(connectionToRemove);
                }
            }
            else //Node <- Transition
            {
                Connection connectionToRemove = transition.outputConnectionList.Find(x => x.node == node);
                if (connectionToRemove != null)
                {
                    connectionToRemove.RemoveConnections();
                    this.connectionList.Remove(connectionToRemove);
                }
            }
        }
        #endregion

        //Marker Handling
        #region MarkerHandling
        public void SetMarker(string nodeName, int markerCount = 1)
        {
            Node node = nodeList.Find(x => x.name == nodeName);
            if (node != null)
            {
                node.SetMarker(markerCount);
            }
        }

        public void SetMarker(Node node, int markerCount = 1)
        {
            node.SetMarker(markerCount);
        }

        public void SetMarker(Node node, Marker newMarker)
        {
            node.SetMarker(newMarker);
        }

        public void SerMarkers(Node node, List<Marker> newMarkerList)
        {
            node.SetMarkers(newMarkerList);
        }

        public void AddMarker(string nodeName, int markerCount = 1)
        {
            Node node = nodeList.Find(x => x.name == nodeName);
            if (node != null)
            {
                node.AddMarker(markerCount);
            }
        }

        public void AddMarker(Node node, int markerCount = 1)
        {
            node.AddMarker(markerCount);
        }

        public void AddMarker(Node node, Marker newMarker)
        {
            node.AddMarker(newMarker);
        }

        public void AddMarkers(Node node, List<Marker> newMarkerList)
        {
            node.AddMarkers(newMarkerList);
        }

        public int GetMarkerCount(string name)
        {
            Node node = nodeList.Find(x => x.name == name);
            if (node != null)
            {
                return node.markerList.Count;
            }
            else
            {
                return -1;
            }
        }

        public int GetMarkerCount(Node node)
        {
            return node.markerList.Count;
        }

        #endregion

        public bool GetTransitionStatus(string name)
        {
            Transition transition = this.transitionList.Find(x => x.name == name);
            if (transition != null)
            {
                return transition.status;
            }
            else
            {
                return false;
            }
        }

        public void SaveNetwork()
        {

        }

        public void LoadNetwork()
        {

        }

        public void CheckConcurrency()
        {
            foreach (Node node in nodeList)
            {
                node.CheckConcurrency();
            }
        }

        public void CheckTransitions()
        {
            //Update Transitions status
            foreach (Transition transition in transitionList)
            {
                transition.UpdateStatus();
            }
        }

        public void CheckNetwork()
        {
            CheckConcurrency();

            CheckTransitions();
        }

        public void Execute() //Execute a cicle
        {
            if (!stopExecution)
            {
                CheckConcurrency();

                CheckTransitions();

                //Execute possible Transitions
                for (int i = 0; i < transitionList.Count; i++)
                {
                    transitionList[i].Execute();
                }

                //Execute possible nodes
                for (int i = 0; i < nodeList.Count; i++)
                {
                    nodeList[i].Execute();
                }
            }
        }

        #region CallbackFunctions
        public void AddNodeOnAddCallback(Node node, Action callback)
        {
            node.onAddCallbackList.Add(callback);
        }

        public void AddNodeOnExecuteCallback(Node node, Action callback)
        {
            node.onExecuteCallbackList.Add(callback);
        }

        public void AddNodeOnSubtractCallback(Node node, Action callback)
        {
            node.onSubtractCallbackList.Add(callback);
        }

        public void AddTransitionCallback(Transition transition, Action callback)
        {
            transition.callbackList.Add(callback);
        }
        #endregion

        #region Concurrency Handler Function

        //This was moved to the marker class as each marker can have its own judging criteria
        /*public static void DefaultConcurrencyHandler(List<Connection> connectionList)
        {
            //Return that the first connection is the one to be chosen
            callback.Invoke(connectionList[0].transition);
        }

        public static void DefaultRandomConcurrencyHandler(List<Connection> connectionList)
        {
            callback.Invoke(connectionList[UnityEngine.Random.Range(0, connectionList.Count - 1)].transition);
        }*/

        #endregion
    }
    #endregion
}
