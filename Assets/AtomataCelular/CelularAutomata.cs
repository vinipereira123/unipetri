﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace CelularAutomata
{
    public enum CellState
    {
        Dead = 0,
        Alive
    }

    public enum EcosystemState
    {
        Dead = 0,
        Alive,
        Upgrade
    }

    public class CelularAutomata : MonoBehaviour
    {
        public int range = 2;
        public static int ecosystemSize = 32;
        public CellState[,] ecosystem, lastEcosystem;
        public bool perlin = true;
        public static float noiseFactorQ;
        public static float noiseScale;
        public static int fillPercent;
        public static int iterationNumber = -1;
        public int iterationUntilDead = 3;
        public int ecosystemLevel = 0;
        public int seed;
        public Action<CellState[,], int> dieFunction;

        public static bool changedOnLastIteration = false;

        public EcosystemState ecosystemState = EcosystemState.Alive;

        int sameEcosystemCount;

        int iterationCount;

        static System.Random rnd = new System.Random(System.DateTime.Now.Millisecond);

        public CelularAutomata(int neighborsRange, int _ecosystemSize, Action<int, int, Action<CellState>> fillFunction, Action<CellState[,], int> _dieFunction, int _seed = 0, int iterCount = -1, int fillP = 50, bool userPerlin = true, float perlinScale = 0.5f)
        {
            ecosystemSize = _ecosystemSize;
            this.range = neighborsRange;
            this.perlin = userPerlin;
            noiseScale = perlinScale;
            noiseFactorQ = fillP;
            fillPercent = fillP;
            iterationNumber = iterCount;
            dieFunction = _dieFunction;
            seed = _seed;

            ecosystem = new CellState[ecosystemSize, ecosystemSize];
            lastEcosystem = new CellState[ecosystemSize, ecosystemSize];

            for (int y = 0; y < ecosystemSize; y++)
            {
                for (int x = 0; x < ecosystemSize; x++)
                {
                    fillFunction(x, y, delegate (CellState newState)
                    {
                        ecosystem[x, y] = newState;
                    });
                }
            }
        }

        public void AutomataUpdate(List<Action<int, CellState, Action<CellState>>> lifeFunctionList, Action updateCallback = null, Action deadCallback = null)
        {
            switch (ecosystemState)
            {
                case EcosystemState.Alive:

                    if ((iterationNumber == -1 || iterationCount < iterationNumber))
                    {
                        changedOnLastIteration = false;

                        CellState[,] nextEcosystem = new CellState[ecosystemSize, ecosystemSize];

                        for (int y = 0; y < ecosystemSize; y++)
                        {
                            for (int x = 0; x < ecosystemSize; x++)
                            {

                                int count = 0; //neightbour count

                                int halfRange = Mathf.FloorToInt(range / 2);

                                //Neightbourings
                                for (int offsetY = y - halfRange; offsetY <= y + halfRange; offsetY++)
                                {
                                    for (int offsetX = x - halfRange; offsetX <= x + halfRange; offsetX++)
                                    {
                                        if (offsetX != 0 && offsetY != 0)
                                        {
                                            int localX = (offsetX + ecosystemSize) % ecosystemSize;
                                            int localY = (offsetY + ecosystemSize) % ecosystemSize;

                                            if (ecosystem[localX, localY] == CellState.Alive)
                                            {
                                                count++;
                                            }
                                        }
                                    }
                                }
                                
                                lifeFunctionList[ecosystemLevel](count, ecosystem[x, y], delegate (CellState newState)
                                {
                                    nextEcosystem[x, y] = newState;

                                    if(nextEcosystem[x,y] != ecosystem[x,y])
                                    {
                                        changedOnLastIteration = true;
                                    }
                                });
                            }
                        }

                        //lastEcosystem = ecosystem;
                        ecosystem = nextEcosystem;
                        
                        //if (CompareEcosystems(lastEcosystem, ecosystem))
                        if(!changedOnLastIteration)
                        {
                            sameEcosystemCount++;
                        }

                        //Check ecosystem state
                        if(sameEcosystemCount >= iterationUntilDead)
                        {
                            //Check if there is an upgrade available
                            if(ecosystemLevel < lifeFunctionList.Count)
                            {
                                ecosystemState = EcosystemState.Alive;
                                sameEcosystemCount = 0;
                                ecosystemLevel++;
                            }
                            else
                            {
                                ecosystemState = EcosystemState.Dead;
                            }
                        }
                        else
                        {
                            ecosystemState = EcosystemState.Alive;
                        }

                        if (iterationNumber > -1)
                        {
                            iterationCount++;
                        }

                        if (updateCallback != null)
                        {
                            updateCallback.Invoke();
                        }
                    }
                    else
                    {
                        //Check if there is an upgrade available
                        if (ecosystemLevel < lifeFunctionList.Count - 1)
                        {
                            ecosystemState = EcosystemState.Alive;
                            sameEcosystemCount = 0;
                            iterationCount = 0;
                            ecosystemLevel++;
                        }
                        else
                        {
                            ecosystemState = EcosystemState.Dead;
                        }
                    }

                    break;

                case EcosystemState.Dead:
                    if (deadCallback != null)
                        deadCallback.Invoke();
                    break;

                case EcosystemState.Upgrade:

                    break;
            }

            if (iterationCount + 1 == iterationNumber)
            {
                ecosystemState = EcosystemState.Dead;
            }
        }

        public bool CompareEcosystems(CellState[,] eco1, CellState[,] eco2) // true equal | false different
        {
            bool tmp = true;
            
            for (int y = 0; y < ecosystemSize; y++)
            {
                for (int x = 0; x < ecosystemSize; x++)
                {
                    if(eco1[x,y] != eco2[x,y])
                    {
                        tmp = false;
                        break;
                    }
                }

                if(!tmp)
                {
                    break;
                }
            }

            return tmp;
        }

        public static void GameOfLife(int count, CellState state, Action<CellState> callback)
        {
            /*
                Any live cell with fewer than two live neighbours dies, as if by underpopulation.
                Any live cell with two or three live neighbours lives on to the next generation.
                Any live cell with more than three live neighbours dies, as if by overpopulation.
                Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
            */

            CellState newState = CellState.Dead;

            if (state == CellState.Alive)
            {
                if (count < 2)
                {
                    newState = CellState.Dead;
                }
                else if (count <= 3)
                {
                    newState = CellState.Alive;
                }
                else
                {
                    newState = CellState.Dead;
                }
            }
            else //Dead
            {
                if (count == 3)
                {
                    newState = CellState.Alive;
                }
            }

            callback.Invoke(newState);
        }

        public static void Caves(int count, CellState state, Action<CellState> callback)
        {
            CellState newState = CellState.Dead;

            if (state == CellState.Alive)
            {
                if (count > 4)
                {
                    newState = CellState.Alive;
                }
                else if (count < 4)
                {
                    newState = CellState.Dead;
                }
            }

            callback.Invoke(newState);
        }

        public static void CavesFillDeadPixels(int count, CellState state, Action<CellState> callback)
        {
            CellState newState = CellState.Dead;

            if (state == CellState.Dead)
            {
                if (count >= 5)
                {
                    newState = CellState.Alive;
                }
            }
            else
            {
                newState = CellState.Alive;
            }

            callback.Invoke(newState);
        }


        #region FILL FUNCTIONS

        public static void RandomFill(int x, int y, Action<CellState> callback)
        {
            CellState newState = CellState.Dead;

            newState = (rnd.Next(0, 100) > fillPercent) ? CellState.Dead : CellState.Alive;

            callback.Invoke(newState);
        }

        //Perlin 
        public static void PerlinFill(int x, int y, Action<CellState> callback)
        {
            CellState newState = CellState.Dead;

            newState = (Mathf.RoundToInt(Mathf.PerlinNoise(x * noiseScale, y * noiseScale) + noiseFactorQ) == 0) ? CellState.Dead : CellState.Alive;

            callback.Invoke(newState);
        }

        public static void CaveFillFunction(int x, int y, Action<CellState> callback)
        {
            CellState newState = CellState.Dead;

            if (x == 0 || x == ecosystemSize - 1 || y == 0 || y == ecosystemSize - 1)
            {
                newState = CellState.Alive;
            }
            else
            {
                newState = (rnd.Next(0, 100) > fillPercent) ? CellState.Dead : CellState.Alive;
            }

            callback.Invoke(newState);
        }

        #endregion
    }
}
