﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniPetri;

public class PlayerBehaviour : MonoBehaviour
{
    public Marker markerToFollow;

    public Vector2 directionToGo;
    public Vector3 positionToGo;
    public Quaternion rotationToGo;
    public float rotationSpeed = 2;
    public float moveSpeed = 2;

    public int gemCount;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Update()
    {
        if (this.directionToGo.x == 1) //right
        {
            rotationToGo = Quaternion.Euler(new Vector3(0, 90, 0));
        }
        else if (this.directionToGo.x == -1) //left
        {
            rotationToGo = Quaternion.Euler(new Vector3(0, 270, 0));
        }
        else if (this.directionToGo.y == 1) //up
        {
            rotationToGo = Quaternion.Euler(new Vector3(0, 0, 0));
        }
        else if (this.directionToGo.y == -1) //down
        {
            rotationToGo = Quaternion.Euler(new Vector3(0, 180, 0));
        }

        this.transform.rotation = Quaternion.Lerp(this.transform.rotation, rotationToGo, rotationSpeed * Time.deltaTime);
        this.transform.position = Vector3.Lerp(this.transform.position, positionToGo, moveSpeed * Time.deltaTime);

        this.UpdatePosition();
    }

    public void ConcurrencyHandler(List<Connection> connectionList)
    {
        this.markerToFollow.chosenTransition = null;

        for (int i = 0; i < connectionList.Count; i++)
        {
            if (connectionList[i].transition.outputConnectionList[0].node.mapTileReference.celularAutomataIndex == this.markerToFollow.parentNode.mapTileReference.celularAutomataIndex + directionToGo)
            {
                this.positionToGo = this.markerToFollow.parentNode.mapTileReference.tileObject.transform.position;
                this.markerToFollow.chosenTransition = connectionList[i].transition;
            }
        }

        if (this.markerToFollow.chosenTransition != null)
        {
            this.markerToFollow.chosenTransition.markerToMove.Add(this.markerToFollow);
        }

        this.directionToGo = Vector2.zero;
    }

    public void UpdatePosition()
    {
        if (markerToFollow != null)
            if (markerToFollow.parentNode != null)
                this.transform.position = markerToFollow.parentNode.mapTileReference.tileObject.transform.position;
    }
}
