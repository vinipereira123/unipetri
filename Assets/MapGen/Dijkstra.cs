﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Priority_Queue;
using System.Linq;

namespace DijkstraAlgorithm
{

    [System.Serializable]
    public class IntVec
    {
        public int x;
        public int y;
        public int z;

        public IntVec(int _x = -1, int _y = -1, int _z = -1)
        {
            this.x = _x;
            this.y = _y;
            this.z = _z;
        }
    }

    [System.Serializable]
    public class PathStepData
    {
        public int id;
        public int cost;
        public IntVec coordinates;
        public IntVec nextCoordinates;

        public PathStepData(int _cost, IntVec _coords, int mapSize)
        {
            this.id = GetId(_coords.x, _coords.y, mapSize);
            this.cost = _cost;
            this.coordinates = _coords;
            this.nextCoordinates = new IntVec();
        }

        public static int GetId(int x, int y, int mapSize)
        {
            return x + (y * mapSize);
        }
    }

    class PathDataCloserOrderer : IComparer<int>
    {
        public int Compare(int x, int y)
        {
            if (x == -1) //This object needs to go down in the list 
            {
                return -1;
            }

            if (x < y)
            {
                return 1;
            }
            else if (x == y)
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
    }

    public class Dijkstra
    {
        public static List<PathStepData> FindPath(int[,] costMatrix, int mapSize, IntVec startPosition, IntVec endPosition)
        {
            if (costMatrix[startPosition.x, startPosition.y] == -1)
            {
                return null;
            }

            List<PathStepData> distanceList = new List<PathStepData>();

            List<PathStepData> pathList = new List<PathStepData>();

            List<int> usedPathIdList = new List<int>();

            for (int addY = 0; addY < mapSize; addY++)
            {
                for (int addX = 0; addX < mapSize; addX++)
                {
                    if ((addX == startPosition.x && addY == startPosition.y - 1) ||
                       (addX == startPosition.x && addY == startPosition.y + 1) ||
                       (addX == startPosition.x - 1 && addY == startPosition.y) ||
                       (addX == startPosition.x + 1 && addY == startPosition.y))
                    {
                        if (costMatrix[addX, addY] > -1)
                        {
                            distanceList.Add(new PathStepData(costMatrix[addX, addY], new IntVec(addX, addY), mapSize));
                            //Adds the data to path list too
                            pathList.Add(new PathStepData(-1, new IntVec(addX, addY), mapSize));
                        }
                    }
                    else if (addX == startPosition.x && addY == startPosition.y)
                    {
                        distanceList.Add(new PathStepData(0, new IntVec(addX, addY), mapSize));
                        //Adds the data to path list too
                        pathList.Add(new PathStepData(-1, new IntVec(addX, addY), mapSize));
                    }
                    else
                    {
                        distanceList.Add(new PathStepData(int.MaxValue, new IntVec(addX, addY), mapSize));
                        //Adds the data to path list too
                        pathList.Add(new PathStepData(-1, new IntVec(addX, addY), mapSize));
                    }
                }
            }

            List<PathStepData> orderedPivotIdList = new List<PathStepData>(distanceList);
            orderedPivotIdList.Sort(delegate (PathStepData x, PathStepData y)
            {
                if (x.cost > -1 && y.cost > -1)
                {
                    return -1;
                }
                else
                {
                    if(x.cost < y.cost)
                    {
                        return 1;
                    }
                    else if(x.cost == y.cost)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            });

            int firstPivotId = orderedPivotIdList[0].id;
            int pivotId = orderedPivotIdList[0].id;

            usedPathIdList.Add(pivotId);

            int listSize = mapSize * mapSize;
            for (int i = 0; i < listSize; i++) //Iteration Loop
            //while(pivotId != distanceList[PathStepData.GetId(endPosition.x, endPosition.y, mapSize)].id)
            {
                //Pass through the possible next pivots to see if it minimizes
                for (int j = 0; j < distanceList.Count; j++)
                {
                    if (!usedPathIdList.Contains(distanceList[j].id))
                    {
                        if ((distanceList[j].coordinates.x == distanceList[pivotId].coordinates.x && distanceList[j].coordinates.y == distanceList[pivotId].coordinates.y - 1) ||
                            (distanceList[j].coordinates.x == distanceList[pivotId].coordinates.x && distanceList[j].coordinates.y == distanceList[pivotId].coordinates.y + 1) ||
                            (distanceList[j].coordinates.x == distanceList[pivotId].coordinates.x - 1 && distanceList[j].coordinates.y == distanceList[pivotId].coordinates.y) ||
                            (distanceList[j].coordinates.x == distanceList[pivotId].coordinates.x + 1 && distanceList[j].coordinates.y == distanceList[pivotId].coordinates.y))
                        {
                            int sum = distanceList[pivotId].cost + costMatrix[distanceList[j].coordinates.x, distanceList[j].coordinates.y];

                            if (sum < distanceList[j].cost)
                            {
                                distanceList[j].cost = sum;
                                pathList[j].id = pivotId;
                                pathList[j].nextCoordinates = distanceList[pivotId].coordinates;
                            }
                        }
                    }
                }

                //Add the not used pivot to the list
                orderedPivotIdList = new List<PathStepData>();
                foreach (PathStepData p in distanceList)
                {
                    if (!usedPathIdList.Contains(p.id))
                    {
                        orderedPivotIdList.Add(p);
                    }
                }

                orderedPivotIdList.Sort(delegate (PathStepData x, PathStepData y)
                {
                    if (x.cost > -1 && y.cost > -1)
                    {
                        return -1;
                    }
                    else
                    {
                        if (x.cost < y.cost)
                        {
                            return 1;
                        }
                        else if (x.cost == y.cost)
                        {
                            return 0;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                });
                if(orderedPivotIdList.Count == 0)
                {
                    break;
                }

                pivotId = orderedPivotIdList[0].id;

                usedPathIdList.Add(pivotId);
            }
            
            //Organize route
            List<PathStepData> finalRouteList = new List<PathStepData>();
            
            return RouteForIndex(startPosition, endPosition, finalRouteList, pathList, new IntVec());
        }

        public static List<PathStepData> RouteForIndex(IntVec startPos, IntVec endPos, List<PathStepData> finalRoute, List<PathStepData> map, IntVec localCurrentPos = null)
        {
            if (localCurrentPos.x == -1 &&
                localCurrentPos.y == -1 &&
                localCurrentPos.z == -1)
            {
                localCurrentPos.x = endPos.x;
                localCurrentPos.y = endPos.y;
            }
            
            PathStepData currentStep = map.Find(z => z.coordinates.x == localCurrentPos.x && z.coordinates.y == localCurrentPos.y);
            if (currentStep == null)
            {
                Debug.LogError("ERROR: The step on coordinate: " + localCurrentPos.x + "|" + localCurrentPos.y + " was not found in the map list.");
                return null;
            }
            PathStepData nextStep = map.Find(z => z.coordinates.x == currentStep.nextCoordinates.x && z.coordinates.y == currentStep.nextCoordinates.y);
            finalRoute.Add(nextStep);
            localCurrentPos.x = currentStep.coordinates.x;
            localCurrentPos.y = currentStep.coordinates.y;

            //Stop criteria
            if(localCurrentPos == startPos)
            {
                finalRoute.Reverse();
                return finalRoute;
            }
            else
            {
                return RouteForIndex(startPos, endPos, finalRoute, map, localCurrentPos);
            }
        }

        //Returns the id of the lowerest value
        /*public static int GetShortestFromList(List<CostData> list)
        {
            int shortestIndex = 0;

            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (list[i].value < list[shortestIndex].value && list[i].value != -1 && list[i].used != true)
                    {
                        shortestIndex = i;
                    }
                }
            }

            return shortestIndex;
        }*/

        //Returns if a given position is inside the map boundries.
        public static bool CheckPositionValidity(int x, int y, int mapSize)
        {
            bool tmp = false;

            if ((x > 0 && x < mapSize) && (y > 0 && y < mapSize))
            {
                tmp = true;
            }

            return tmp;
        }

    }
}
