﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniPetri;
using System;
using UnityEngine.Events;

[System.Serializable]
public class LifeCycleEvent
{
    public string eventName;
    public int cycleNumber;
    public UnityEvent callback;
}

public class EnemyBehaviour : MonoBehaviour
{
    Animator anim;

    public Marker markerToFollow;

    public int lifetimeCycleCount;
    public int cycleDuration;
    public int cycleTimer;
    public float transitionTime = 1;

    [Header("Overtime events")]
    public List<LifeCycleEvent> lifeCycleEventList;

    public Transform target;

    Vector3 posToGo, oldPosToGo;

    // Start is called before the first frame update
    void Start()
    {
        anim = this.GetComponent<Animator>();
        cycleTimer = cycleDuration;

    }

    public void ConcurrencyHandler(List<Connection> connectionList)
    {
        if (cycleTimer <= 0)
        {
            if (markerToFollow != null)
            {
                this.markerToFollow.chosenTransition = null;
                List<Transition> possibleTransitions = new List<Transition>();

                foreach (Connection c in connectionList)
                {
                    if (c.transition.outputConnectionList[0].node.markerList.Count == 0)
                    {
                        possibleTransitions.Add(c.transition);
                    }
                }

                float distance = float.MaxValue;
                Transition selected = null;

                foreach (Transition t in possibleTransitions)
                {
                    float localDistance = Vector3.Distance(t.outputConnectionList[0].node.mapTileReference.tileObject.transform.position, target.position);
                    if(localDistance < distance)
                    {
                        distance = localDistance;
                        selected = t;
                    }
                }

                if (selected != null)
                {
                    this.markerToFollow.chosenTransition = selected;
                    this.markerToFollow.chosenTransition.markerToMove.Add(this.markerToFollow);
                }
            }

            //Add lifetime cycle count
            lifetimeCycleCount++;

            //Reset timer
            cycleTimer = cycleDuration;
        }
        else
        {
            cycleTimer--;
        }

        foreach (LifeCycleEvent lce in lifeCycleEventList)
        {
            if (lifetimeCycleCount == lce.cycleNumber)
            {
                lce.callback.Invoke();
            }
        }
    }

    public void UpdatePosition()
    {
        if (markerToFollow != null)
            if (markerToFollow.parentNode != null)
            {
                posToGo = markerToFollow.parentNode.mapTileReference.tileObject.transform.position;
                this.transform.LookAt(posToGo);
            }
    }

    private void Update()
    {
        if(posToGo != oldPosToGo)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, posToGo, Time.deltaTime);

            if(this.transform.position == posToGo)
            {
                oldPosToGo = posToGo;
            }
        }
    }

    public void DeathEvent()
    {
        anim.SetTrigger("Death");
        if (markerToFollow != null)
        {
            markerToFollow.SelfRemove();
            markerToFollow = null;
        }
        Destroy(this.gameObject, 10);
    }
}
