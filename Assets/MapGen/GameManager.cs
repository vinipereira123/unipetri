﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniPetri;
using System;

namespace GameLoop
{

    /*
     * Fazer personagens decidir para onde ir ou o que fazer após N ciclos da rede de petri (precisa bloquear as decisoes até o ultimo ciclo)
     * Seguir PDF Dijkstra do professor.
     * 
     */

    [System.Serializable]
    public class LevelData
    {
        public int size;
        public float spawnIntervalMin;
        public float spawnIntervalMax;
        public int startEnemyCount;
        public int maxEnemyCount;
    }

    public class GameManager : MonoBehaviour
    {
        public MapGenerator map;
        public GameObject tilePrefab;

        public float timer;

        public GameObject enemyPrefab;
        public List<EnemyBehaviour> enemyList = new List<EnemyBehaviour>();
        public GameObject playerPrefab;
        public PlayerBehaviour player;

        public GameObject gemPrefab;
        public List<GemBehaviour> gemList = new List<GemBehaviour>();

        [Header("UI Refs")]
        public TMPro.TextMeshProUGUI gemCountText;

        [Header("Game Vars")]
        public int mapFullness = 65;
        public int enemyCount = 10;
        public int gemCount = 3;
        public float cycleInterval;
        [Range(1, 100)]
        public float spawnIntervalMin;
        public float spawnIntervalMax;
        public bool stopSpawning = false;

        Vector3 cameraPosition;

        public int currentLevel = 0;
        public List<LevelData> levelList;

        bool finished = false;

        // Start is called before the first frame update
        void Start()
        {
            cameraPosition = Camera.main.transform.position;

            GenerateLevel();

            /*   Original
             *   5,1,0
            *    3,1,1
            *    2,4,-1
            */
            List<DijkstraAlgorithm.PathStepData> path = DijkstraAlgorithm.Dijkstra.FindPath(new int[,] { {5,3,2 }, //0
                                                                                                         {1,1,4 }, //1
                                                                                                         {0,1,-1 } //2
                                                                                                       }, 3, new DijkstraAlgorithm.IntVec(1, 2), new DijkstraAlgorithm.IntVec(0, 2));

            //Organizer route sequence


            Debug.Log("AHA");
        }

        public void RouteToIndex()
        {

        }

        public void GenerateLevel()
        {
            //Generate Map

            map = new MapGenerator(levelList[this.currentLevel].size, this.mapFullness);
            map.GenerateMap(tilePrefab, delegate ()
            {
                //Create Player
                player = GameObject.Instantiate(playerPrefab).GetComponent<PlayerBehaviour>();
                Node newPlayerFirstNode = map.mapPN.GetRandomNode();
                Marker newPlayerMarker = new Marker(newPlayerFirstNode, player.gameObject, player.ConcurrencyHandler, null);
                player.markerToFollow = newPlayerMarker;
                //Crate player marker in the petri net
                map.mapPN.AddMarker(newPlayerFirstNode, newPlayerMarker);

                Camera.main.gameObject.GetComponent<CameraBehaviour>().transformToFollow = this.player.transform;

                player.UpdatePosition();

                StartCoroutine(SpawnEnemy(levelList[currentLevel].startEnemyCount, true));
                stopSpawning = true;

                for (int i = 0; i < gemCount; i++)
                {
                    GemBehaviour newGem = GameObject.Instantiate(gemPrefab).GetComponent<GemBehaviour>();

                    Node referenceNode = map.mapPN.GetRandomNode();

                    Vector3 gemPosition = referenceNode.mapTileReference.tileObject.transform.position;

                    gemPosition.y = 1;

                    newGem.Spawn(gemPosition, delegate (GemBehaviour gem)
                    {
                        this.player.gemCount++;
                        gemList.Remove(gem);
                    });

                    gemList.Add(newGem);
                }
            });
        }

        public void CreateEnemy()
        {
            //Create the enemy object in the world
            EnemyBehaviour newEnemy = GameObject.Instantiate(enemyPrefab).GetComponent<EnemyBehaviour>();
            newEnemy.target = this.player.transform;
            this.enemyList.Add(newEnemy);
            Node newEnemyFirstNode = map.mapPN.GetRandomNode();
            Marker newEnemyMarker = new Marker(newEnemyFirstNode, newEnemy.gameObject, newEnemy.ConcurrencyHandler, newEnemy.UpdatePosition);
            newEnemy.markerToFollow = newEnemyMarker;
            //Crate enemy marker in the petri net
            map.mapPN.AddMarker(newEnemyFirstNode, newEnemyMarker);
            newEnemy.UpdatePosition();
        }

        public IEnumerator SpawnEnemy(int amount = 5, bool now = false)
        {
            float timeToWait = Random(System.DateTime.Now.Second, this.levelList[currentLevel].spawnIntervalMin, this.levelList[currentLevel].spawnIntervalMax);

            //Amount clamp
            if(this.enemyList.Count + amount > this.levelList[currentLevel].maxEnemyCount)
            {
                amount = this.levelList[currentLevel].maxEnemyCount - this.enemyList.Count;
            }

            if (now)
            {
                for (int i = 0; i < amount; i++)
                {
                    CreateEnemy();
                }
            }

            yield return new WaitForSecondsRealtime(timeToWait);

            if (!now)
            {
                //Spawn here
                for (int i = 0; i < amount; i++)
                {
                    CreateEnemy();
                }
            }

            if (!this.stopSpawning)
            {
                StartCoroutine(SpawnEnemy(Random((int)timeToWait, 1, 10)));
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!this.finished)
            {
                if (this.player != null)
                {
                    UpdateUI();
                }

                //Runs the map logic
                map.mapPN.Execute();

                //Player input
                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    player.directionToGo.y = 1;
                    player.directionToGo.x = 0;
                }
                else
                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    player.directionToGo.y = -1;
                    player.directionToGo.x = 0;
                }
                else
                if (Input.GetKeyDown(KeyCode.RightArrow))
                {
                    player.directionToGo.x = 1;
                    player.directionToGo.y = 0;
                }
                else
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                {
                    player.directionToGo.x = -1;
                    player.directionToGo.y = 0;
                }

                if (gemList.Count <= 0)
                {
                    if (this.currentLevel + 1 < this.levelList.Count)
                    {
                        this.currentLevel++;
                        DestroyAll();
                        GenerateLevel();
                    }
                    else
                    {
                        CameraBehaviour.finalRotation = true;
                        this.finished = true;
                    }
                }
            }
        }

        public void DestroyAll()
        {
            StopAllCoroutines();

            if (this.enemyList.Count > 0)
            {
                foreach (EnemyBehaviour enemy in enemyList)
                {
                    Destroy(enemy.gameObject);
                }

                this.enemyList.Clear();
            }

            //Clean sutff if neeeded
            Destroy(this.player.gameObject);

            if (this.gemList.Count > 0)
            {
                foreach (GemBehaviour gem in gemList)
                {
                    Destroy(gem.gameObject);
                }

                this.gemList.Clear();
            }

            map.SelfDestroy();
        }

        public void UpdateUI()
        {
            this.gemCountText.text = "Gems: " + this.player.gemCount.ToString();
        }

        public int Random(int seed, int min = 0, int max = 1)
        {
            int r = (int)XORShift(seed);
            return (r < min) ? min : (r > max) ? max : r;
        }

        public float Random(int seed, float min = 0, float max = 1)
        {
            float r = (float)XORShift(seed);
            return (r < min) ? min : (r > max) ? max : r;
        }

        public float XORShift(int seed)
        {
            byte x = Convert.ToByte(seed);
            x ^= (byte)(x << 21);
            x ^= (byte)(x >> 35);
            x ^= (byte)(x << 4);
            return x;
        }
    }
}
