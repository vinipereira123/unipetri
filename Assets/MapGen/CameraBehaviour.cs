﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public Vector3 offsetPosition;
    public Vector3 offsetRotation;
    public Transform transformToFollow;

    public static bool finalRotation = false;

    // Update is called once per frame
    void Update()
    {
        if (this.transformToFollow != null)
        {
            this.transform.position = transformToFollow.position + offsetPosition;
            this.transform.rotation = Quaternion.Euler(offsetRotation);
        }

        if(finalRotation)
        {
            //this.transform.RotateAround(this.transformToFollow.position, Vector3.up, this.transform.eulerAngles.y + 10);
        }
    }
}
