﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GemBehaviour : MonoBehaviour
{
    Action<GemBehaviour> gatherFunction;

    public void Spawn(Vector3 position, Action<GemBehaviour> _gatherFunction)
    {
        this.gatherFunction = _gatherFunction;

        this.transform.position = position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            gatherFunction.Invoke(this);

            //Explodes!
            Destroy(this.gameObject);
        }
    }
}
