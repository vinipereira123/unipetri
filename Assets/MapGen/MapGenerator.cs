﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UniPetri;
using CelularAutomata;

namespace GameLoop
{
    [System.Serializable]
    public class MapSector
    {
        public int id;
        public int width = 0, height = 0;
        public int ancorX = 0, ancorY = 0;
        public List<Vector2> tilePositionList = new List<Vector2>();
        public Color color;

        public MapSector(int _id)
        {
            this.id = _id;
            this.color = UnityEngine.Random.ColorHSV();//new Color(UnityEngine.Random.Range(0,255), UnityEngine.Random.Range(0, 255), UnityEngine.Random.Range(0, 255));
        }
    }

    public class MapGenerator : MonoBehaviour
    {
        public int mapSize = 8;
        public PetriNetwork mapPN;
        CelularAutomata.CelularAutomata mapCA;

        [Range(0, 100)]
        public int fillPercent = 50;
        //public bool usePerlin = true;
        //public float perlinScale = 0.5f;
        [Range(-1, 10)]
        public int iterationNumber = 5;

        public Transform mapParent;

        public List<MapSector> mapSectorList = new List<MapSector>();
        public MapTile[,] mapTileList;
        public int[,] sectorIdMap;

        public GameObject tilePrefab;

        [Header("GEN")]
        public bool regen;

        public void CleanMap()
        {
            foreach (Transform tile in mapParent)
            {
                Destroy(tile.gameObject);
            }
        }

        public void SelfDestroy()
        {
            mapPN.stopExecution = true;
            Destroy(this.mapParent.gameObject);
            Destroy(this);
        }

        public MapGenerator(int mSize, int fPercent = 65)
        {
            this.mapSize = mSize;

            this.mapTileList = new MapTile[mapSize, mapSize];
            this.sectorIdMap = new int[mapSize, mapSize];

            this.mapParent = new GameObject("Map").transform;

            this.fillPercent = fPercent;

            //GenerateMap();
        }

        public void GenerateMap(GameObject tPrefab, Action OnFinished)
        {
            this.tilePrefab = tPrefab;

            GenerateCA();

            //Map ground or void in sector map
            for (int y = 0; y < mapSize; y++)
            {
                for (int x = 0; x < mapSize; x++)
                {
                    if (mapCA.ecosystem[x, y] == CellState.Alive)
                    {
                        sectorIdMap[x, y] = -1;
                    }
                    else
                    {
                        sectorIdMap[x, y] = -99;
                    }
                }
            }

            //Create map locomotion PetriNet
            mapPN = new PetriNetwork();

            CreateMapSectors();

            //The CreateMapSector creates the nodes in the tiles, so we can connect them
            foreach (MapSector ms in mapSectorList)
            {
                for (int i = 0; i < ms.tilePositionList.Count; i++)
                {
                    Vector3 position = ms.tilePositionList[i];
                    Node currentNode = GetTileAtPos((int)position.x, (int)position.y).pnNode;
                    //Get possible peers
                    List<UniPetri.Node> peerNodeList = new List<Node>();

                    if (CheckAliveTile((int)position.x, (int)position.y + 1))//Up
                    {
                        peerNodeList.Add(GetTileAtPos((int)position.x, (int)position.y + 1).pnNode);
                    }

                    if (CheckAliveTile((int)position.x + 1, (int)position.y))//Right
                    {
                        peerNodeList.Add(GetTileAtPos((int)position.x + 1, (int)position.y).pnNode);
                    }

                    if (CheckAliveTile((int)position.x, (int)position.y - 1))//Down
                    {
                        peerNodeList.Add(GetTileAtPos((int)position.x, (int)position.y - 1).pnNode);
                    }

                    if (CheckAliveTile((int)position.x - 1, (int)position.y))//Left
                    {
                        peerNodeList.Add(GetTileAtPos((int)position.x - 1, (int)position.y).pnNode);
                    }

                    //Connect possible peers
                    for (int j = 0; j < peerNodeList.Count; j++)
                    {
                        mapPN.CreateConnection(currentNode, peerNodeList[j]);
                    }
                }
            }

            //TODO
            //Connect Sectors with Bridge

            //Run code when finished
            OnFinished.Invoke();
        }

        private void Update()
        {
            if (regen)
            {
                DestroyImmediate(mapParent.gameObject);

                GenerateMap(this.tilePrefab, null);

                regen = false;
            }
        }

        public void GenerateCA()
        {
            mapCA = new CelularAutomata.CelularAutomata(2, mapSize, CelularAutomata.CelularAutomata.CaveFillFunction, null, 0, iterationNumber, fillPercent);

            for (int i = 0; i < iterationNumber; i++)
            {
                mapCA.AutomataUpdate(new List<System.Action<int, CellState, System.Action<CellState>>>() { CelularAutomata.CelularAutomata.Caves, CelularAutomata.CelularAutomata.CavesFillDeadPixels }, null, null);
            }
        }

        public void FillSector(int x, int y, List<Vector2> sectorPositionsList, int sectorId)
        {
            Vector2 newPos = new Vector2(x, y);
            if (!sectorPositionsList.Contains(newPos))
            {
                //Set this tile in the list to be filled afterwards
                sectorPositionsList.Add(newPos);

                sectorIdMap[x, y] = sectorId;

                mapTileList[x, y] = new MapTile();
                mapTileList[x, y].Initialize(tilePrefab, mapParent, x, y, mapSectorList[sectorId].color, mapPN);

                if (CheckValidPosition(x, y + 1) && sectorIdMap[x, y + 1] == -1) //up
                {
                    FillSector(x, y + 1, sectorPositionsList, sectorId);
                }

                if (CheckValidPosition(x + 1, y) && sectorIdMap[x + 1, y] == -1) //right
                {
                    FillSector(x + 1, y, sectorPositionsList, sectorId);
                }

                if (CheckValidPosition(x, y - 1) && sectorIdMap[x, y - 1] == -1) //down
                {
                    FillSector(x, y - 1, sectorPositionsList, sectorId);
                }

                if (CheckValidPosition(x - 1, y) && sectorIdMap[x - 1, y] == -1) //left
                {
                    FillSector(x - 1, y, sectorPositionsList, sectorId);
                }
            }
        }

        public void CreateMapSectors() //Create the Map Sectors
        {
            //Flood and fill sectors
            for (int y = 0; y < mapSize; y++)
            {
                for (int x = 0; x < mapSize; x++)
                {
                    if (sectorIdMap[x, y] == -1)
                    {
                        //Create new Sector!
                        MapSector newSector = new MapSector(mapSectorList.Count);
                        mapSectorList.Add(newSector);

                        List<Vector2> sectorTilePositionList = new List<Vector2>();
                        FillSector(x, y, sectorTilePositionList, newSector.id);
                        newSector.tilePositionList = new List<Vector2>(sectorTilePositionList);
                    }
                }
            }
        }

        public MapTile GetTileAtPos(int x, int y)
        {
            if ((x > -1 && x < this.mapSize) && (y > -1 && y < this.mapSize))
            {
                return mapTileList[x, y];
            }
            else
            {
                return null;
            }
        }

        public bool CheckValidPosition(int x, int y)
        {
            if ((x > -1 && x < this.mapSize) && (y > -1 && y < this.mapSize))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool CheckAliveTile(int x, int y)
        {
            if (CheckValidPosition(x, y))
            {
                if (mapCA.ecosystem[x, y] == CellState.Alive)
                {
                    return true;
                }
            }

            return false;
        }

        public MapSector CreateSector(int x, int y)
        {
            MapSector newSector = new MapSector(mapSectorList.Count);

            mapSectorList.Add(newSector);

            return newSector;
        }

        public static void MapConcurrencyHandler(List<Connection> connectionList, Action<string> callback)
        {
            //Return that the first connection is the one to be chosen
            callback.Invoke(connectionList[0].transition.uniqueId);
        }
    }
}
