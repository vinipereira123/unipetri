﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTile : MonoBehaviour
{
    public bool active;
    public Vector2 celularAutomataIndex;
    public GameObject tileObject;
    public int sectorId = -1;

    public UniPetri.Node pnNode;
    public List<UniPetri.Node> peerPnNodeList;

    public void Initialize(GameObject prefab, Transform parent, int x, int y, Color color, UniPetri.PetriNetwork pnNetwork, bool isActive = true)
    {
        if (isActive)
        {
            this.celularAutomataIndex = new Vector2(x, y);

            tileObject = GameObject.Instantiate(prefab, parent);
            tileObject.transform.position = new Vector3(x, 0, y);

            tileObject.transform.GetComponentInChildren<MeshRenderer>().material.SetColor("_Color", color);

            this.active = isActive;
        }
        else
        {
            this.active = false;
        }

        pnNode = pnNetwork.CreateNode(this, x+","+y);
    }
}
